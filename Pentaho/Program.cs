﻿using MySql.Data.MySqlClient;
using Pentaho.Modelos;
using System;
using System.Collections;

namespace Pentaho
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Variables

            MySqlCommand codigo = Coneccion.Coneccion.Conexion();

            string strLinea = "";


            #endregion


            #region ListaNombrePersonas
            ArrayList listaNombrePersonas = new ArrayList();

            listaNombrePersonas.Add("Juan");
            listaNombrePersonas.Add("Carlos");
            listaNombrePersonas.Add("Andres");
            listaNombrePersonas.Add("Alex");
            listaNombrePersonas.Add("John");
            listaNombrePersonas.Add("Daniel");
            listaNombrePersonas.Add("David");
            listaNombrePersonas.Add("Franklin");
            listaNombrePersonas.Add("Enrique");
            listaNombrePersonas.Add("Yadira");
            listaNombrePersonas.Add("Eugenia");
            listaNombrePersonas.Add("Robin");
            listaNombrePersonas.Add("Lenin");
            listaNombrePersonas.Add("Vanessa");
            listaNombrePersonas.Add("Nicole");
            listaNombrePersonas.Add("Karolina");
            listaNombrePersonas.Add("Karol");
            listaNombrePersonas.Add("Carla");
            listaNombrePersonas.Add("Davis");
            listaNombrePersonas.Add("Gerardo");
            listaNombrePersonas.Add("Rcardo");
            listaNombrePersonas.Add("Erick");
            listaNombrePersonas.Add("Patricia");
            listaNombrePersonas.Add("Vicky");
            listaNombrePersonas.Add("Paulina");
            listaNombrePersonas.Add("Paula");
            listaNombrePersonas.Add("Francisco");
            listaNombrePersonas.Add("Elizabeth");
            listaNombrePersonas.Add("Soraya");
            listaNombrePersonas.Add("Carlota");
            listaNombrePersonas.Add("Stefany");
            listaNombrePersonas.Add("Gabriel");
            listaNombrePersonas.Add("Gabriela");
            listaNombrePersonas.Add("Cristina");
            listaNombrePersonas.Add("Pablo");
            listaNombrePersonas.Add("Franco");
            listaNombrePersonas.Add("Jorge");
            listaNombrePersonas.Add("Ivan");
            listaNombrePersonas.Add("Marco");
            listaNombrePersonas.Add("Mercy");
            listaNombrePersonas.Add("Esperanza");



            #endregion

            #region ListaApellidoPersonas

            ArrayList listaApellidoPersonas = new ArrayList();

            listaApellidoPersonas.Add("Alarcon");
            listaApellidoPersonas.Add("Ochoa");
            listaApellidoPersonas.Add("Chamba");
            listaApellidoPersonas.Add("Macas");
            listaApellidoPersonas.Add("Soto");
            listaApellidoPersonas.Add("Rodriguez");
            listaApellidoPersonas.Add("Cordova");
            listaApellidoPersonas.Add("Alvarado");
            listaApellidoPersonas.Add("Lopez");
            listaApellidoPersonas.Add("Salcedo");
            listaApellidoPersonas.Add("Salgado");
            listaApellidoPersonas.Add("Valdivieso");
            listaApellidoPersonas.Add("Maldonado");
            listaApellidoPersonas.Add("Aguirre");
            listaApellidoPersonas.Add("Ramirez");
            listaApellidoPersonas.Add("Esparza");
            listaApellidoPersonas.Add("Perez");
            listaApellidoPersonas.Add("Garcia");
            listaApellidoPersonas.Add("Hernandez");
            listaApellidoPersonas.Add("Cisneros");
            listaApellidoPersonas.Add("Gomez");
            listaApellidoPersonas.Add("Vazques");
            listaApellidoPersonas.Add("Veintimilla");
            listaApellidoPersonas.Add("Criollo");
            listaApellidoPersonas.Add("Medina");
            listaApellidoPersonas.Add("Ojeda");
            listaApellidoPersonas.Add("Araujo");
            listaApellidoPersonas.Add("Carrion");
            listaApellidoPersonas.Add("Sanchez");
            listaApellidoPersonas.Add("Nole");
            listaApellidoPersonas.Add("Pilco");
            listaApellidoPersonas.Add("Granda");
            listaApellidoPersonas.Add("Galvan");
            listaApellidoPersonas.Add("Galan");
            listaApellidoPersonas.Add("Calvachi");
            listaApellidoPersonas.Add("Figueroa");
            listaApellidoPersonas.Add("Pesantez");
            listaApellidoPersonas.Add("Ortega");
            listaApellidoPersonas.Add("Soto");
            listaApellidoPersonas.Add("Mera");
            listaApellidoPersonas.Add("Troya");
            listaApellidoPersonas.Add("Flores");
            listaApellidoPersonas.Add("Calvas");
            listaApellidoPersonas.Add("Poma");
            listaApellidoPersonas.Add("Ruales");
            listaApellidoPersonas.Add("Jara");
            listaApellidoPersonas.Add("Gonzalez");
            listaApellidoPersonas.Add("Bolt");
            listaApellidoPersonas.Add("Valencia");
            listaApellidoPersonas.Add("Preciado");
            listaApellidoPersonas.Add("Hurtado");

            #endregion

            #region Proceso

            Random rNombrePersonas = new Random();

            Random rApellidoPersonas = new Random();

            Random rDireccion = new Random();

            Random rEdad = new Random();

            ArrayList listaClientes = new ArrayList();

            string fija = "@gmail.com";

            int i = 0;

            codigo = Coneccion.Coneccion.Conexion();

            while ( i < 2500)
            {  
                string nombre = listaNombrePersonas[rNombrePersonas.Next(0, listaNombrePersonas.Count)].ToString();

                string apellido = listaApellidoPersonas[rNombrePersonas.Next(0, listaNombrePersonas.Count)].ToString();

                Clientes cliente = new Clientes();

                cliente.nombre = nombre;

                cliente.apellido = apellido;

                cliente.direccion = rDireccion.Next(1, 47);               

                cliente.Edad = rEdad.Next(18, 59);

                cliente.correoElectronico = nombre + apellido + cliente.Edad + fija;

                if (!listaClientes.Contains(cliente))
                {
                    i++;

                    listaClientes.Add(cliente);
                    try
                    {

                        strLinea = "insert into clientes ( nombre, apellido, idDireccion, correoElectronico, edad ) " +
                            "values ('" + cliente.nombre + "' , '" + cliente.apellido + "' , '" + cliente.direccion + "' , '" + cliente.correoElectronico + "' , '" + cliente.Edad + "' )";

                        codigo.CommandText = (strLinea);

                        codigo.ExecuteNonQuery();

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        
                    }
                    
                }
                else
                {

                    Console.WriteLine("*******************************************************");

                }
                
            }

            Console.WriteLine(listaClientes.Count);
            
            Console.ReadKey();

        



            #endregion




        }
    }
}
