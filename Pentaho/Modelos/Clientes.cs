﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentaho.Modelos
{
    class Clientes
    {
        #region Propiedades
        public string nombre { get; set; }

        public string apellido { get; set; }

        public int direccion { get; set; }

        public string correoElectronico { get; set; }

        public int Edad { get; set; }

        #endregion


        #region Metodo

        public override string ToString()
        {
            return nombre + apellido + direccion + correoElectronico + Edad;
        }

        #endregion

    }
}
