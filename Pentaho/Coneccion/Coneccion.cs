﻿using MySql.Data.MySqlClient;


namespace Pentaho.Coneccion
{
    class Coneccion
    {
        static MySqlConnection coneeccion;

        public static MySqlCommand Conexion()
        {

            coneeccion = new MySqlConnection("server=127.0.0.1; database=pentaho; Uid=root; pwd=root;");

            coneeccion.Open();

            MySqlCommand codigo = new MySqlCommand();

            codigo.Connection = coneeccion;

            return codigo;

        }

        public static void cerrarConexion()
        {

            coneeccion.Close();
        }

    }
}
